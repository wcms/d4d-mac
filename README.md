# Docker 4 Drupal on M1 revisited
___
New way of using docker4drupal project on Apple Silicon chips.

### Requirements
* Docker version 20.10.7
* Docker Desktop 4.5.0 (Colima as alternative)
* Mutagen version 0.13.1
* Mutagen-compose version 0.13.1

If your system have previous version of mutagen upgrade it, and install mutagen
compose

`brew install mutagen-io/mutagen/mutagen`

`brew install mutagen-io/mutagen/mutagen-compose`

Versions of mutagen and mutagen-compose must match and be at least 0.13.0.

### Steps for new project
* Clone the repository
* Update .env file with unique project name, set PHP version.
* For multiple projects, update docker-compose.yml mounts, and traefik ports.
* Run `mutagen-compose up -d` this will download docker images and crate
  containers, establish mutagen link for file sync.
* (recommended) Copy `default.bashrc` file to html/ folder.
* (recommended) Enter php container with `mutagen-compose exec php bash`
* (recommended) Once inside a container run: `source default.bashrc` followed by
  `rcsetup` and `init` to prepare environment for wcms3 work. The whole process
  take few mins to complete. It will switch version of composer, install all
  needed packages.
* (recommended) After run `wcms3` which is command alias, this will clone wcms
  drupal recommended project to you html/ folder.
* (recommended) Finally run `./rebuild.sh 2 2` this will kick of wcms profile
  build. This may take more than 10 min to complete.


### Start existing project
* Navigate to project folder (where docker-compose.yml file is)
* Run `mutagen-compose up -d`


### Stop running project
* To stop project run `mutagen-compose stop` this can be done at the end of day.
  You can start project next day.

### To delete/destroy project
* Run `mutagen-compose down` this will delete all containers and networks
  created by docker. Your code will remain in html/ folder.
* To start with clean slate, you could delete html/ folder and use steps for new
  project.


## Some special cases
___
### Colors from terminal are gone
This means your PHP container was recreated from docker image and all settings
defaulted. To fix this exit the container, copy `default.bashrc` to html/
folder. Enter the php container and do source/rcsetup/init again.
