# Global helper aliases
alias ll='ls -alhF --color=auto'
PS1='\[\033[0;32m\]\[\033[0m\033[0;32m\]\u\[\033[0;35m\]@\[\033[0;35m\]${WODBY_APP_NAME:-php}.${WODBY_ENVIRONMENT_NAME:-container} \[\033[0;32m\]\w\[\033[0;33m\]\n\[\033[0;32m\]└─\[\033[0m\033[0;32m\] \$\[\033[0m\033[0;32m\]\[\033[0m\] '
alias rcsetup='mv ./default.bashrc ~/ && echo -e "\n[[ -f ~/default.bashrc ]] && source ~/default.bashrc\n" >> ~/.bashrc'
alias inpm='sudo apk update && sudo apk add npm python2 alpine-sdk && sudo npm install -g gulp'
alias ics='composer global require "squizlabs/php_codesniffer=*" && composer global require drupal/coder && composer global require dealerdirect/phpcodesniffer-composer-installer'
alias ..='cd ../'
alias ..2='cd ../../'
alias ..3='cd ../../../'
alias ..4='cd ../../../../'
alias ..5='cd ../../../../../'
alias ..6='cd ../../../../../../'
alias ..7='cd ../../../../../../../'
alias ..8='cd ../../../../../../../../'

# Drupal generic aliases
alias d9si='drush si --db-url=mysql://drupal:drupal@mariadb/drupal'
alias d8rp='composer create-project drupal/recommended-project:^8 ./'
alias d9rp='composer create-project drupal/recommended-project ./'

# WCMS 3 specific aliases
alias d9uw='drush si uw_base_profile --db-url=mysql://drupal:drupal@mariadb/drupal -y'
alias runcs='phpcs --standard=Drupal,DrupalPractice --report-width=150 --extensions=php,module,inc,install,test,profile,theme,js,info,txt,md,scss,css,sh --ignore=*.min.js,*.min.css .'
alias runcsfix='phpcbf --standard=Drupal,DrupalPractice --report-width=150 --extensions=php,module,inc,install,test,profile,theme,js,info,txt,md,scss,css,sh --ignore=*.min.js,*.min.css .'
alias runcsphp='phpcs --standard=PHPCompatibility --runtime-set testVersion 7.3- --report-width=150 --extensions=php,module,inc,install,test,profile,theme .'
alias wcms3='git clone https://git.uwaterloo.ca/wcms/drupal-recommended-project.git .'
alias init='composer self-update --1 && composer global remove drush/drush && composer global require drush/drush:^10 && composer global require twig/twig:1.42.5 && inpm'

# WCMS Tools
alias uwwcmstools='git clone https://git.uwaterloo.ca/wcms/uw_wcms_tools.git /home/wodby/uw_wcms_tools'
